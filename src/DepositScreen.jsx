import React, { useState, useEffect } from 'react';
import RetirementManagerContract from './RetirementManagerContract';
import web3 from 'web3';

function DepositScreen() {
  const [depositAmount, setDepositAmount] = useState(0);

  const handleDeposit = async (event) => {
    event.preventDefault();
    const contract = RetirementManagerContract;
   //  const accounts = await web3.eth.getAccounts();
    await contract.methods.deposit().send({
      from: window.ethereum.selectedAddress,
      value: web3.utils.toWei(depositAmount, 'ether'),
    });
    setDepositAmount(0);
  };

  return (
    <div>
      <h1>Depósito</h1>
      <form onSubmit={handleDeposit}>
        <label>
          Cantidad a depositar:
          <input
            type="number"
            step="0.0001"
            value={depositAmount}
            onChange={(event) => setDepositAmount(event.target.value)}
          />
        </label>
        <button type="submit">Depositar</button>
      </form>
    </div>
  );
}

export default DepositScreen;
