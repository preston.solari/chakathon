import React, { useState, useEffect } from 'react';
import RetirementManagerContract from './RetirementManagerContract';
import web3 from 'web3';

function HomeScreen() {
  const [owner, setOwner] = useState('');
  const [balance, setBalance] = useState(0);
  const [beneficiariesCount, setBeneficiariesCount] = useState(0);

  useEffect(() => {
    async function fetchData() {
      const contract = RetirementManagerContract;
      const _owner = await contract.methods.owner().call();
      const _beneficiaries = await contract.methods.getBeneficiaries().call({ from: window.ethereum.selectedAddress });
      const _balance = await contract.methods.getBalance().call({ from: window.ethereum.selectedAddress });
      setOwner(_owner);
      setBalance(web3.utils.fromWei(_balance, 'ether'));
      setBeneficiariesCount(_beneficiaries[0].length);
    }
    fetchData();
  }, []);

  return (
    <div>
      <h1>Contrato de Jubilación</h1>
      <p>Propietario: {owner}</p>
      <p>Saldo Total: {balance}</p>
      <p>Número de Beneficiarios: {beneficiariesCount}</p>
    </div>
  );
}

export default HomeScreen;
