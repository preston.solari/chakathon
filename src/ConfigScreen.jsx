import React, { useState, useEffect } from 'react';
import RetirementManagerContract from './RetirementManagerContract';

window.RetirementManagerContract = RetirementManagerContract;

function ConfigScreen() {
  const [retirementAge, setRetirementAge] = useState(0);
  const [retirementPayDay, setRetirementPayDay] = useState(0);
  const [beneficiaries, setBeneficiaries] = useState([]);
  const [distributionPercentages, setDistributionPercentages] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => {
    
    const connectWallet = async () => {      
      if (window.ethereum) {
        const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
        const address = accounts[0];
        console.log("Mi Address", address);
      } else {
        // El usuario no tiene una billetera compatible
      }
    }
    connectWallet();

    async function fetchData() {
      const contract = RetirementManagerContract;
      const _retirementAge = await contract.methods.getRetirementAge().call({ from: window.ethereum.selectedAddress });
      const _retirementPayDay = await contract.methods.getRetirementPayDay().call({ from: window.ethereum.selectedAddress });
      var _beneficiaries = await contract.methods.getBeneficiaries().call({ from: window.ethereum.selectedAddress });
      const _distributionPercentages = _beneficiaries[1];
      _beneficiaries = _beneficiaries[0];
      // const _distributionPercentages = await contract.methods.getDistributionPercentages().call();

      console.log("beneficiaries", _beneficiaries);
      console.log("distributionPercentages", _distributionPercentages);
      
      setRetirementAge(_retirementAge);
      setRetirementPayDay(_retirementPayDay);
      setBeneficiaries(_beneficiaries);
      setDistributionPercentages(_distributionPercentages);
    }
    fetchData();
  }, []);

  async function handleSaveConfig() {
    try {
      const contract = RetirementManagerContract;
      await contract.methods.setRetirementAge(retirementAge).send({ from: window.ethereum.selectedAddress });
      await contract.methods.setRetirementPayDay(retirementPayDay).send({ from: window.ethereum.selectedAddress });
      await contract.methods.setBeneficiaries(beneficiaries, distributionPercentages).send({ from: window.ethereum.selectedAddress });
    } catch (error) {
      setError(error.message);
    }
  }

  function handleBeneficiaryChange(event, index) {
    const updatedBeneficiaries = [...beneficiaries];
    updatedBeneficiaries[index] = event.target.value;
    setBeneficiaries(updatedBeneficiaries);
  }

  function handleDistributionChange(event, index) {
    const updatedDistributionPercentages = [...distributionPercentages];
    updatedDistributionPercentages[index] = event.target.value;
    setDistributionPercentages(updatedDistributionPercentages);
  }

  function addBeneficiary() {
    setBeneficiaries([...beneficiaries, '']);
    setDistributionPercentages([...distributionPercentages, 0]);
  }

  return (
    <div>
      <h1>Configuración</h1>
      <p>Edad de Jubilación:</p>
      <input type="number" value={retirementAge} onChange={(e) => setRetirementAge(e.target.value)} />
      <p>Día de Pago de Jubilación:</p>
      <input type="number" value={retirementPayDay} onChange={(e) => setRetirementPayDay(e.target.value)} />
      <p>Beneficiarios:</p>
      {beneficiaries.map((beneficiary, index) => (
        <div key={index}>
          <input type="text" value={beneficiary} onChange={(e) => handleBeneficiaryChange(e, index)} />
          <input type="number" value={distributionPercentages[index]} onChange={(e) => handleDistributionChange(e, index)} />
        </div>
      ))}
      <button onClick={addBeneficiary}>Agregar Beneficiario</button>
      <button onClick={handleSaveConfig}>Guardar Configuración</button>
      {error && <p>Error: {error}</p>}
    </div>
  );
}

export default ConfigScreen;
