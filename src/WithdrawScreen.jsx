import React, { useState } from 'react';
import RetirementManagerContract from './RetirementManagerContract';
import web3 from 'web3';

function WithdrawScreen() {
  const [amount, setAmount] = useState(0);
  const [error, setError] = useState(null);

  async function handleWithdraw() {
    try {
      const contract = RetirementManagerContract;
      await contract.methods.withdraw(web3.utils.toWei(amount, 'ether')).send({ from: window.ethereum.selectedAddress });
    } catch (error) {
      setError(error.message);
    }
  }

  function handleAmountChange(event) {
    setAmount(event.target.value);
  }

  return (
    <div>
      <h1>Retirar Fondos</h1>
      <p>Ingrese la cantidad que desea retirar:</p>
      <input type="number" value={amount} onChange={handleAmountChange} />
      <button onClick={handleWithdraw}>Retirar</button>
      {error && <p>Error: {error}</p>}
    </div>
  );
}

export default WithdrawScreen;
