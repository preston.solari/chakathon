import Web3 from 'web3';
import RetirementManager from './contracts/RetirementManager.json';

const web3 = new Web3(Web3.givenProvider || 'http://localhost:8545');
const contractAddress = '0xc37B5f160B8f09A4EB9E523208e0EAc215d9Feb8';

console.log(RetirementManager);
const RetirementManagerContract = new web3.eth.Contract(RetirementManager, contractAddress);

export default RetirementManagerContract;