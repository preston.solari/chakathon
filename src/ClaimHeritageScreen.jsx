import React, { useState } from 'react';
import RetirementManagerContract from './RetirementManagerContract';

function ClaimHeritageScreen() {
  const [account, setAccount] = useState('');
  const [message, setMessage] = useState('');

  const handleInputChange = (event) => {
    setAccount(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const contract = RetirementManagerContract;
      await contract.methods.requestHeritage(account).send({ from: window.ethereum.selectedAddress });
      setMessage('Heritage requested successfully!');
    } catch (error) {
      setMessage(error.message);
    }
  };

  return (
    <div>
      <h1>Reclamar Herencia</h1>
      <form onSubmit={handleSubmit}>
        <label>
          Cuenta:
          <input type="text" value={account} onChange={handleInputChange} />
        </label>
        <button type="submit">Solicitar Herencia</button>
      </form>
      <p>{message}</p>
    </div>
  );
}

export default ClaimHeritageScreen;
