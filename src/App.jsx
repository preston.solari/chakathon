import React, { useEffect } from 'react';
import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom';
import HomeScreen from './HomeScreen';
import ConfigScreen from './ConfigScreen';
import WithdrawScreen from './WithdrawScreen';
import EmergencyScreen from './EmergencyScreen';
import DepositScreen from './DepositScreen';
import ClaimHeritageScreen from './ClaimHeritageScreen';
import './App.css';



function App() {
  return (
    <Router>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/">Inicio</Link>
            </li>
            <li>
              <Link to="/config">Configuración</Link>
            </li>
            <li>
              <Link to="/deposit">Depositar Fondos</Link>
            </li>
            <li>
              <Link to="/withdraw">Retirar Fondos</Link>
            </li>
            <li>
              <Link to="/emergency">Retiro de Emergencia</Link>
            </li>
            <li>
              <Link to="/heritage">Reclamar Herencia</Link>
            </li>
          </ul>
        </nav>

        <Routes>
          <Route path="/config" element={<ConfigScreen />}/>
          <Route path="/deposit" element={<DepositScreen />}/>
          <Route path="/withdraw" element={<WithdrawScreen />}/>
          <Route path="/emergency" element={<EmergencyScreen />}/>
          <Route path="/heritage" element={<ClaimHeritageScreen />}/>
          <Route path="/" element={<HomeScreen />}/>
        </Routes>
      </div>
    </Router>
  );
}

export default App;
