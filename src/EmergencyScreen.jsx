import React, { useState } from 'react';
import RetirementManagerContract from './RetirementManagerContract';

function EmergencyScreen() {
  const [error, setError] = useState(null);

  async function handleEmergencyWithdraw() {
    try {
      const contract = RetirementManagerContract;
      await contract.methods.emergencyWithdrawal().send({ from: window.ethereum.selectedAddress });
    } catch (error) {
      setError(error.message);
    }
  }

  return (
    <div>
      <h1>Retiro de Emergencia</h1>
      <p>Este retiro solo se puede realizar en caso de emergencia.</p>
      <p>Los fondos se distribuirán entre los beneficiarios según las proporciones establecidas.</p>
      <button onClick={handleEmergencyWithdraw}>Retirar</button>
      {error && <p>Error: {error}</p>}
    </div>
  );
}

export default EmergencyScreen;
