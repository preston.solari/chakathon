pragma solidity ^0.7.0;

contract RetirementManager {
    address public owner;
    mapping(address => uint256) public balances;
    mapping(address => uint256) public keepAlive;
    mapping(address => address[]) public beneficiaries;
    mapping(address => uint256[]) public distributionPercentages;
    mapping(address => uint256) public retirementAge;
    mapping(address => uint256) public retirementPayDay;
    mapping(address => mapping(address => bool)) public heritageRequested;


    event BeneficiariesUpdated(
        address indexed account,
        address[] beneficiaries,
        uint256[] distributionPercentages
    );

    constructor() {
        owner = msg.sender;
    }

    function deposit() public payable {
        require(msg.value > 0, "Deposit amount must be greater than 0.");
        balances[msg.sender] += msg.value;
        keepAlive[msg.sender] = block.timestamp + 30 days;
    }

    function withdraw(uint256 amount) public {
        require(amount <= balances[msg.sender], "Insufficient balance.");
        require(
            block.timestamp < keepAlive[msg.sender],
            "You need to be alive to withdraw your funds."
        );
        balances[msg.sender] -= amount;
        payable(msg.sender).transfer(amount);
    }

    function requestHeritage(address account) public {
        require(
            block.timestamp >= keepAlive[account],
            "The account owner needs to be deceased to request the heritage."
        );

        require(
            heritageRequested[account][msg.sender] == false,
            "Heritage already requested by this beneficiary."
        );

        uint256 totalPercentage = 0;
        for (uint256 i = 0; i < beneficiaries[account].length; i++) {
            totalPercentage += distributionPercentages[account][i];
        }

        uint256 totalHeritage = balances[account];
        for (uint256 i = 0; i < beneficiaries[account].length; i++) {
            address beneficiary = beneficiaries[account][i];
            uint256 percentage = distributionPercentages[account][i];
            uint256 amount = (totalHeritage * percentage) / 100;
            balances[account] -= amount;
            balances[beneficiary] += amount;
        }

        heritageRequested[account][msg.sender] = true;
    }


    function setBeneficiaries(
        address[] memory _beneficiaries,
        uint256[] memory _distributionPercentages
    ) public {
        require(
            _beneficiaries.length == _distributionPercentages.length,
            "Lengths of beneficiaries and distributionPercentages arrays do not match."
        );
        uint256 totalPercentage = 0;
        for (uint256 i = 0; i < _distributionPercentages.length; i++) {
            totalPercentage += _distributionPercentages[i];
        }
        require(totalPercentage == 100, "Percentages must add up to 100.");
        beneficiaries[msg.sender] = _beneficiaries;
        distributionPercentages[msg.sender] = _distributionPercentages;

        emit BeneficiariesUpdated(
            msg.sender,
            _beneficiaries,
            _distributionPercentages
        );
    }

    function getBeneficiaries() public view returns (address[] memory, uint256[] memory) {
        return (beneficiaries[msg.sender], distributionPercentages[msg.sender]);
    }

    function getDistributionPercentages() public view returns (uint256[] memory) {
        return distributionPercentages[msg.sender];
    }

    function setRetirementAge(uint256 _retirementAge) public {
        retirementAge[msg.sender] = _retirementAge;
    }

    function setRetirementPayDay(uint256 _retirementPayDay) public {
        retirementPayDay[msg.sender] = _retirementPayDay;
    }

    function getRetirementAge() public view returns (uint256) {
        return retirementAge[msg.sender];
    }

    function getRetirementPayDay() public view returns (uint256) {
        return retirementPayDay[msg.sender];
    }

    function emergencyWithdrawal() public {
        uint256 balance = balances[msg.sender];
        require(balance > 0, "Insufficient balance.");
        balances[msg.sender] = 0;
        payable(msg.sender).transfer(balance);
    }

    function getBalance() public view returns (uint256) {
        return balances[msg.sender];
    }
}
